# ViaCep


Teste - QA VR

Teste:
1 - Criar uma funcionalidade para consultar os dados de um endereço a partir de um CEP.
2 - Cenários:    
    2.1 - Criar um cenário de sucesso na consulta, printando o código do IBGE do endereço no stdout.
    2.2 - Criar um cenário passando um CEP inválido

Dicas:
Utilizar a API https://viacep.com.br/ws/01001000/json/ para consulta;
Gem HTTParty pode ser uma ajuda incrível para trabalhar com as requisições HTTP




